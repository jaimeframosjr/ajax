<?php
		$servername = "localhost";
		$username = "root";
		$password = "";
		$database = "testing";
		// Create connection
		$conn = new mysqli($servername, $username, $password,$database);

		// Check connection
		if ($conn->connect_error) {
		    die("Connection failed: " . $conn->connect_error);
		} 

		$sql = "SELECT name FROM categories";
		$result = $conn->query($sql);	
		$conn->close();
?>
<!DOCTYPE html>
<html>
<head>
<title>Page Title</title>
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" >
</head>
<body>
	<div class="container">
		<div class="col-lg-3">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<div class="panel-title">Categories</div>
				</div>
				<div class="panel-body">
					<select name="categories" id="categories" class="form-control">
						<?php foreach($result as $d):?>
						  		<option value="<?php echo $d['name'];?>"><?php echo $d['name'];?></option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>
		</div>
		
	    <div class="col-lg-3">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<div class="panel-title">Drinks</div>
				</div>
				<div class="panel-body">
					<select name="drink" id="drink" class="form-control"></select>
				</div>
				
				</div>
		</div>
	</div>
<script src="js/jquery-3.2.1.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="js/myjs.js"></script>
</body>
</html>